package com.evaluator.evaluator.config;

import com.evaluator.evaluator.service.UpdateSender;
import com.evaluator.evaluator.service.providers.HolidaysProvider;
import com.evaluator.evaluator.service.providers.RushHoursProvider;
import com.evaluator.evaluator.service.providers.WeatherProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import java.io.IOException;

@Configuration
@ConfigurationProperties
@EnableScheduling
@EnableConfigurationProperties
public class StartupConfigurator {
    @Autowired
    HolidaysProvider holidaysProvider;

    @Autowired
    WeatherProvider weatherProvider;

    @Autowired
    RushHoursProvider rushHoursProvider;

    @Autowired
    UpdateSender updateSender;

    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludeQueryString(true);
        loggingFilter.setIncludePayload(true);
        return loggingFilter;
    }
    @Bean
    public void updateAndVerifyHolidays() throws IOException {
        holidaysProvider.updateLocalHolidays();
        holidaysProvider.verifyIsWorkingDay();
    }

    @Bean
    public void updateAndVerifyWeather() throws IOException {
        System.out.println("updating weather");
        weatherProvider.updateAndVerifyWeather();
    }

    @Bean
    public void verifyRushHours() {
        rushHoursProvider.verifyIsRushHour();
    }

    @Bean
    public void informClientAboutDelay() {
        updateSender.informClientUpdateDate();
    }


}
