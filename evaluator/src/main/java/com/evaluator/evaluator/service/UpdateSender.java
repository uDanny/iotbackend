package com.evaluator.evaluator.service;


import com.evaluator.evaluator.dto.ActionDto;
import com.evaluator.evaluator.dto.enums.Color;
import com.evaluator.evaluator.dto.enums.Operation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.evaluator.evaluator.StaticValues.*;

@Service
public class UpdateSender {
    @Value("${client.url}")
    private String urlResource;

    public void informClientUpdateDate() {
        System.out.println("Sending update rate " + UPDATE_RATE_IN_SEC);
        Map<String, Integer> params = new HashMap<>();
        params.put("rate", UPDATE_RATE_IN_SEC);
        new RestTemplate().exchange(urlResource + "setupInform?rate={rate}", HttpMethod.POST, null, Void.class, params);
    }

    public void sendUpdate() {

        List<ActionDto> actionDtos = new ArrayList<>();
        if (!IS_WORKING_DAY) {
            calculateNonWorkingDay(actionDtos);
        } else if (IS_RUSH_HOUR) {
            calculateRushHour(actionDtos);
        }
        if (IS_EXTREME_WEATHER) {
            calculateWeather(actionDtos);
        }
        System.out.println("Actual States: " + "Is working day: " + IS_WORKING_DAY + " Is extreme weather: " + IS_EXTREME_WEATHER + " Is rush hour: " + IS_RUSH_HOUR);
        System.out.println("Sending update operations " + actionDtos);
        System.out.println(new RestTemplate().postForEntity(urlResource, actionDtos, String.class));
    }

    private void calculateWeather(List<ActionDto> actionDtos) {
        ActionDto vehicleAction = new ActionDto();
        vehicleAction.setCoefficient((double) 1);
        vehicleAction.setColor(Color.YELLOW);
        vehicleAction.setOperation(Operation.INCREASE);
        actionDtos.add(vehicleAction);
    }

    private void calculateRushHour(List<ActionDto> actionDtos) {
        ActionDto vehicleAction = new ActionDto();
        vehicleAction.setCoefficient(2.0);
        vehicleAction.setColor(Color.GREEN);
        vehicleAction.setOperation(Operation.MULTIPLY);
        actionDtos.add(vehicleAction);
    }


    private void calculateNonWorkingDay(List<ActionDto> actionDtos) {
        ActionDto vehicleAction = new ActionDto();
        vehicleAction.setCoefficient(1.2);
        vehicleAction.setColor(Color.GREEN);
        vehicleAction.setOperation(Operation.MULTIPLY);

        actionDtos.add(vehicleAction);
    }
}
