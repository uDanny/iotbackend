package com.evaluator.evaluator.service.providers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.evaluator.evaluator.StaticValues.IS_EXTREME_WEATHER;

@Service
public class WeatherProvider {
    @Value("${api.weather.key}")
    public String API_KEY;
    @Value("${api.weather.city}")
    public String CITY;
    @Value("${api.weather.url}")
    public String URL;

    public void updateAndVerifyWeather() throws IOException {
        ResponseEntity<String> response = new RestTemplate().getForEntity(URL, String.class, getUrlParams());
        JsonNode weatherCode = new ObjectMapper().readTree(response.getBody()).path("weather");
        if (weatherCode.isArray()) {
            List<Boolean> nodesHasBadWeather = new ArrayList<>();
            new ObjectMapper().readTree(response.getBody()).path("weather").elements().forEachRemaining(x -> nodesHasBadWeather.add(x.path("id").asText().matches("([234567]).*")));
            IS_EXTREME_WEATHER = nodesHasBadWeather.stream().anyMatch(x -> x);
        } else {
            IS_EXTREME_WEATHER = weatherCode.path("id").asText().matches("([234567]).*");

        }
    }

    private Map<String, String> getUrlParams() {
        Map<String, String> vars = new HashMap<>();
        vars.put("key", API_KEY);
        vars.put("city", CITY);
        return vars;
    }

}
