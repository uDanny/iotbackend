package com.evaluator.evaluator.service.providers;

import com.evaluator.evaluator.StaticValues;
import com.evaluator.evaluator.dto.HolidayDto;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.evaluator.evaluator.StaticValues.HOLIDAYS;

@Service
public class HolidaysProvider {
    @Value("${api.holidays.key}")
    public String API_KEY;
    @Value("${api.holidays.country}")
    public String COUNTRY;
    @Value("${api.holidays.url}")
    public String URL;// = "https://www.calendarindex.com/api/v1/holidays?country={country}&year={year}&api_key={key}";

    public void updateLocalHolidays() throws IOException {
        System.out.println("updateLocalHolidays");
        ResponseEntity<String> response = new RestTemplate().getForEntity(URL, String.class, getUrlParams());
        HOLIDAYS = getHolidays(response).stream().map(x -> LocalDate.parse(x.getDate().substring(0, 10))).collect(Collectors.toList());
    }

    public void verifyIsWorkingDay() {
        System.out.println("verifyIsWorkingDay");
        LocalDate now = LocalDate.now();
        DayOfWeek dayOfWeek = now.getDayOfWeek();
        StaticValues.IS_WORKING_DAY = !dayOfWeek.equals(DayOfWeek.SATURDAY)
                && !dayOfWeek.equals(DayOfWeek.SUNDAY)
                && !StaticValues.HOLIDAYS.contains(now);
    }

    private Map<String, String> getUrlParams() {
        Map<String, String> vars = new HashMap<>();
        vars.put("country", COUNTRY);
        vars.put("year", String.valueOf(LocalDate.now().getYear()));
        vars.put("key", API_KEY);
        return vars;
    }

    private List<HolidayDto> getHolidays(ResponseEntity<String> response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode holidaysNode = mapper.readTree(response.getBody()).path("response").path("holidays");
        ObjectReader reader = mapper.readerFor(new TypeReference<List<HolidayDto>>() {
        });
        return reader.readValue(holidaysNode);
    }
}
