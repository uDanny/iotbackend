package com.evaluator.evaluator.service.providers;

import com.evaluator.evaluator.StaticValues;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static com.evaluator.evaluator.StaticValues.IS_RUSH_HOUR;

@Service
public class RushHoursProvider {
    private List<RushHours> rushHours = Arrays.asList(
            new RushHours(LocalTime.of(8, 0), LocalTime.of(10, 0)),
            new RushHours(LocalTime.of(17, 0), LocalTime.of(19, 0)));

    public void verifyIsRushHour() {
        LocalTime now = LocalTime.now();
        IS_RUSH_HOUR = rushHours.stream().anyMatch(x -> now.isAfter(x.start) && now.isBefore(x.end));
    }

    @AllArgsConstructor
    private class RushHours {
        private LocalTime start;
        private LocalTime end;
    }
}
