package com.evaluator.evaluator.job;

import com.evaluator.evaluator.service.UpdateSender;
import com.evaluator.evaluator.service.providers.HolidaysProvider;
import com.evaluator.evaluator.service.providers.RushHoursProvider;
import com.evaluator.evaluator.service.providers.WeatherProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JobManager {
    @Autowired
    private UpdateSender updateSender;
    @Autowired
    private HolidaysProvider holidaysProvider;
    @Autowired
    private WeatherProvider weatherProvider;
    @Autowired
    private RushHoursProvider rushHoursProvider;

    @Scheduled(cron = "0,20,40 * * ? * *")//every 20 sec
    public void updateJob() {
        updateSender.sendUpdate();
    }

    @Scheduled(cron = "0 0 0 1/1 * ?") //every month
    public void updateLocalHolidays() throws IOException {
        holidaysProvider.updateLocalHolidays();
    }

    @Scheduled(cron = "0 0 0 ? * *") //every day
    public void verifyIsWorkingDay() throws IOException {
        holidaysProvider.verifyIsWorkingDay();
    }

    @Scheduled(cron = "0 0 * ? * *") //every hour
    public void verifyIsWeather() throws IOException {
        weatherProvider.updateAndVerifyWeather();
    }

    @Scheduled(cron = "0 0 * ? * *") //every hour
    public void verifyIsRushHour() {
        rushHoursProvider.verifyIsRushHour();
    }
}
