package com.evaluator.evaluator.dto.enums;

public enum Operation {
    INCREASE,
    MULTIPLY
}
