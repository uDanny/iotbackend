package com.evaluator.evaluator.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class HolidayDto {
    private String date;
    private String start;
    private String end;
    private String name;
    private String type;
}
