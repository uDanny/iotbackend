package com.evaluator.evaluator.dto;

import com.evaluator.evaluator.dto.enums.Color;
import com.evaluator.evaluator.dto.enums.Operation;
import lombok.Data;

@Data
public class ActionDto {
    private Color color;
    private Double coefficient;
    private Operation operation;
}
