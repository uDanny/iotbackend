package com.evaluator.evaluator.dto.enums;

public enum Color {
    GREEN,
    YELLOW,
    RED
}
