package com.evaluator.evaluator;

import com.evaluator.evaluator.dto.ActionDto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class StaticValues {
    public final static int UPDATE_RATE_IN_SEC = 20;
    public static List<LocalDate> HOLIDAYS = new ArrayList<>();
    public static boolean IS_WORKING_DAY = false;
    public static boolean IS_RUSH_HOUR = false;
    public static boolean IS_EXTREME_WEATHER = false;
}
