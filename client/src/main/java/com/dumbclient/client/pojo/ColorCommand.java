package com.dumbclient.client.pojo;

import com.dumbclient.client.dto.enums.Color;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ColorCommand {
    private Color color;
    private double delay;
}
