package com.dumbclient.client.dto.enums;

public enum Operation {
    INCREASE,
    MULTIPLY
}
