package com.dumbclient.client.dto;

import com.dumbclient.client.dto.enums.Color;
import com.dumbclient.client.dto.enums.Operation;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public class LightMessageDto {
    private Color color;
}
