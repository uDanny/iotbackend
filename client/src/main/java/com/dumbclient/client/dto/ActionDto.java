package com.dumbclient.client.dto;

import com.dumbclient.client.dto.enums.Color;
import com.dumbclient.client.dto.enums.Operation;
import lombok.Data;

@Data
public class ActionDto {
    private Color color;
    private Double coefficient;
    private Operation operation;
}
