package com.dumbclient.client.dto.enums;

public enum Color {
    GREEN,
    YELLOW,
    RED
}
