package com.dumbclient.client.service;

import com.dumbclient.client.dto.ActionDto;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.dumbclient.client.statichelper.StaticLightsBuffer.*;

@Service
public class OperationProcessorService {
    public void processOperations(List<ActionDto> operation) {
        double greenDelay = DEFAULT_GREEN_DELAY;
        double yellowDelay = DEFAULT_YELLOW_DELAY;
        double redDelay = DEFAULT_RED_DELAY;

        for (ActionDto actionDto : operation) {
            switch (actionDto.getColor()) {
                case GREEN:
                    greenDelay = performCalculation(actionDto, greenDelay);
                    break;
                case YELLOW:
                    yellowDelay = performCalculation(actionDto, yellowDelay);
                    break;
                case RED:
                    redDelay = performCalculation(actionDto, redDelay);
                    break;
            }
        }

        ACTUAL_GREEN_DELAY = greenDelay;
        ACTUAL_YELLOW_DELAY = yellowDelay;
        ACTUAL_RED_DELAY = redDelay;
        lastUpdate = LocalDateTime.now();

        System.out.println("After processing operations Actual data: ");
        System.out.println("green " + DEFAULT_GREEN_DELAY + "->" + ACTUAL_GREEN_DELAY);
        System.out.println("yellow " + DEFAULT_YELLOW_DELAY + "->" + ACTUAL_YELLOW_DELAY);
        System.out.println("red " + DEFAULT_RED_DELAY + "->" + ACTUAL_RED_DELAY);
    }

    private double performCalculation(ActionDto actionDto, double actualValue) {
        switch (actionDto.getOperation()) {
            case INCREASE:
                return actualValue + actionDto.getCoefficient();
            case MULTIPLY:
                return actualValue * actionDto.getCoefficient();
        }
        return actualValue;
    }
}
