package com.dumbclient.client.service;

import com.dumbclient.client.pojo.ColorCommand;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static com.dumbclient.client.statichelper.StaticLightsBuffer.*;

@Service
public class LightsUpdaterService {
    public static final double LATENCY_ADMISIBLE_ERROR = 0.5;
    @Value("${lights.url1}")
    private String lightUrl1;
    @Value("${lights.url2}")
    private String lightUrl2;

    public void run() throws InterruptedException {
        while (true) {
            sendUpdate(YELLOW_COMMAND);
            checkEvaluatorIsActual();
            sendUpdate(RED_COMMAND, GREEN_COMMAND);
            sendUpdate(GREEN_COMMAND, RED_COMMAND);
        }
    }

    private void checkEvaluatorIsActual() {
        LocalDateTime now = LocalDateTime.now();
        boolean isEvaluatorDown = UPDATE_RATE_IN_SEC == 0 || lastUpdate == null ||
                now.minusSeconds(UPDATE_RATE_IN_SEC + (int) (UPDATE_RATE_IN_SEC * LATENCY_ADMISIBLE_ERROR))
                .isAfter(lastUpdate);
        System.out.println("Evaluator is actual " + !isEvaluatorDown);
        if (isEvaluatorDown) {
            YELLOW_COMMAND.setDelay(DEFAULT_YELLOW_DELAY);
            GREEN_COMMAND.setDelay(DEFAULT_GREEN_DELAY);
            RED_COMMAND.setDelay(DEFAULT_RED_DELAY);
        }else {
            YELLOW_COMMAND.setDelay(ACTUAL_YELLOW_DELAY);
            GREEN_COMMAND.setDelay(ACTUAL_GREEN_DELAY);
            RED_COMMAND.setDelay(ACTUAL_RED_DELAY);
        }
    }

    private void sendUpdate(ColorCommand command) throws InterruptedException {
        sendUpdate(command, command);
    }

    private void sendUpdate(ColorCommand command1, ColorCommand command2) throws InterruptedException {
        System.out.println(command1);

        Thread thread1 = new Thread() {
            public void run() {
                System.out.println("I am sending to lightUrl1" + command1.getColor());
                try {
                    Map<String, String> params = new HashMap<>();
                    params.put("color", command1.getColor().name().toLowerCase());
                    new RestTemplate().exchange(lightUrl1 + "setupInform?color={color}", HttpMethod.GET, null, Void.class, params);
//                    new RestTemplate().postForEntity(lightUrl1, new LightMessageDto(command1.getColor()), String.class);
                } catch (Exception e) {
                    System.out.println("sending lightUrl2" + command1.getColor() + "exception");
                }
            }
        };
        Thread thread2 = new Thread(() -> {
            System.out.println("I am sending to lightUrl2" + command2.getColor());
            try {
                Map<String, String> params = new HashMap<>();
                params.put("color", command2.getColor().name().toLowerCase());
                new RestTemplate().exchange(lightUrl2 + "setupInform?color={color}", HttpMethod.GET, null, Void.class, params);
//                    new RestTemplate().postForEntity(lightUrl2, new LightMessageDto(command2.getColor()), String.class);
            } catch (Exception e) {
                System.out.println("sending lightUrl2" + command2.getColor() + " exception");
            }
        });
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        long delayCalculating = (long) (command1.getDelay() > command2.getDelay() ? command1.getDelay() : command2.getDelay()) * 1000;
        System.out.println("Delay : " + delayCalculating/1000);
        Thread.sleep(delayCalculating);
    }
}
