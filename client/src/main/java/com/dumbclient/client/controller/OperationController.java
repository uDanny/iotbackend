package com.dumbclient.client.controller;

import com.dumbclient.client.dto.ActionDto;
import com.dumbclient.client.service.OperationProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.dumbclient.client.statichelper.StaticLightsBuffer.UPDATE_RATE_IN_SEC;

@RestController
public class OperationController {
    @Autowired
    private OperationProcessorService processorService;

    @PostMapping(value = "/")
    public Boolean operationsHandling(@RequestBody List<ActionDto> operation) {
        System.out.println("Recieved operations: " + operation);
        processorService.processOperations(operation);
        return true;
    }

    @PostMapping(value = "/setupInform")
    public String setupInformHandling(@RequestParam int rate) {
        System.out.println("Update rate is updated to: " + rate);
        UPDATE_RATE_IN_SEC = rate;
        return "OK";
    }

    @GetMapping(value = "/")
    public String greeting() {
        return "OK";
    }
}
