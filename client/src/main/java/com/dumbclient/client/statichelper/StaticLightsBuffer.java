package com.dumbclient.client.statichelper;

import com.dumbclient.client.dto.enums.Color;
import com.dumbclient.client.pojo.ColorCommand;

import java.time.LocalDateTime;

public class StaticLightsBuffer {

    public static int UPDATE_RATE_IN_SEC;
    public static LocalDateTime lastUpdate;
    public static final double DEFAULT_GREEN_DELAY = 5.0;
    public static final double DEFAULT_YELLOW_DELAY = 3.0;
    public static final double DEFAULT_RED_DELAY = 5.0;

    public static double ACTUAL_GREEN_DELAY = DEFAULT_GREEN_DELAY;
    public static double ACTUAL_YELLOW_DELAY = DEFAULT_YELLOW_DELAY;
    public static double ACTUAL_RED_DELAY = DEFAULT_RED_DELAY;

    public static ColorCommand GREEN_COMMAND = new ColorCommand(Color.GREEN, DEFAULT_GREEN_DELAY);
    public static ColorCommand YELLOW_COMMAND = new ColorCommand(Color.YELLOW, DEFAULT_YELLOW_DELAY);
    public static ColorCommand RED_COMMAND = new ColorCommand(Color.RED, DEFAULT_RED_DELAY);
}
